# Zadani
Nastudujte zakladni praci s knihovnou Twitter4J: (http://twitter4j.org/en/code-examples.html).
Detaily hledejte v JavaDoc dokumentaci: (http://twitter4j.org/javadoc/index.html).

Klice pro 4 uzivatele najdete v adresari twitter_properties. Vyberte si jeden a zkopirujte jej do vychoziho adresare.
Jedna se o uzivatele OMOStudent01, OMOStudent2, OMOStudent3 a OMOStudent4

1. example1(): pouze vyzkousejte spustit. Otestujte zda vse funguje. Dohledejte si pouzite metody v dokumentaci Twitter4J.
2. example2(): napiste novy status. Zacnete sledovat (follow) jineho uzivatele. Pozor, toto funguje pouze pro OMOStudent01.
3. example3(): nastudujte si dokumentaci TwitterStream a doplnte kod OMOStatusListener tak, aby se vypsal kazdy status. O jaky navrhovy vzor jde?
4. example4(): nastudujte si dokumentaci knihovny TexCat (http://textcat.sourceforge.net/) a doplnte kod FilteringOMOStatusListener tak, aby
tiskl jen status psany v anglictine ("english").
5. example5(): doplnte kod LanguageStatisticsOMOStatusListener tak, aby kazdych infoInterval statusu vypsal na radek statistiku zastoupeni jazyka.
 Serazenou sestupne podle poctu vyskytu. Pouzijte nasledujici format:

hungarian: 120 english: 15 spanish: 8 unknown: 5 swedish: 1

# Potrebne knihovny
[Guava](http://search.maven.org/remotecontent?filepath=com/google/guava/guava/17.0/guava-17.0.jar)
[Twitter4J](http://twitter4j.org/archive/twitter4j-4.0.1.zip)
[TextCat](http://sourceforge.net/projects/textcat/files/)

# Upozorneni
Twitter je verejna sluzba: dodrzujte pravidla slusneho chovani, viz (https://edux.feld.cvut.cz/courses/A7B36OMO/plagiatorstvi)
