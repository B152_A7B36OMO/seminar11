package seminar12.solution;

import twitter4j.Status;

public class OMOStatusListener extends twitter4j.StatusAdapter {
    public void onStatus(Status status) {
        System.out.println(status.getUser().getName() + " : " + status.getText());
    }
}
