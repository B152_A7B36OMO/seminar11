package seminar12.solution;

import com.google.common.base.Objects;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.knallgrau.utils.textcat.TextCategorizer;
import twitter4j.Status;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;

public class LanguageStatisticsOMOStatusListener extends twitter4j.StatusAdapter {
    private final TextCategorizer tc = new TextCategorizer();
    private final Map<String, Integer> languageCountMap = Maps.newHashMap();
    private final int infoInterval;
    private int count = 0;

    public LanguageStatisticsOMOStatusListener(int infoInterval) {
        this.infoInterval = infoInterval;
    }

    public void onStatus(Status status) {
        count++;
        String text = status.getText();
        String category = status.getLang();
        languageCountMap.put(category, Objects.firstNonNull(languageCountMap.get(category), 0) + 1);
        if (count % infoInterval == 0) {
            //sort entries by descending count
            Set<Map.Entry<String, Integer>> sortedEntries = Sets.newTreeSet(
                    new Comparator<Map.Entry<String, Integer>>() {
                @Override
                public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                    return -o1.getValue().compareTo(o2.getValue());
                }
            });
            sortedEntries.addAll(languageCountMap.entrySet());
            for (Map.Entry<String, Integer> entry : sortedEntries) {
                System.out.print(entry.getKey() + ": " + entry.getValue() + " ");
            }
            System.out.println();
        }
    }
}
