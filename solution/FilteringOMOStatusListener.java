package seminar12.solution;

import org.knallgrau.utils.textcat.TextCategorizer;
import twitter4j.Status;

public class FilteringOMOStatusListener extends twitter4j.StatusAdapter {
    TextCategorizer tc = new TextCategorizer();

    public void onStatus(Status status) {
        String text = status.getText();
        String category = tc.categorize(text);
        if (category.equals("english"))
            System.out.println(status.getUser().getName() + ": " + text);
    }
}
