package seminar12.solution;

import twitter4j.*;

import java.util.List;

public class Main {
    public static void example1() throws TwitterException {
        Twitter twitter = new TwitterFactory().getInstance();

        System.out.println(twitter);
        System.out.println(twitter.getScreenName());
        List<Status> statuses = twitter.getHomeTimeline();
        for (Status s : statuses) {
            User u = s.getUser();
            System.out.println(u.getName() + ", " + s.getCreatedAt() + ": " + s.getText());
        }
    }

    //TOTO nepujde museli bychom autorizovat telefonem
    public static void example2() throws TwitterException {
        Twitter twitter = new TwitterFactory().getInstance();

        twitter.updateStatus("What a wonderful day!");
        twitter.createFriendship("A7B36OMO");
    }

    public static void example3() throws TwitterException {
        TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
        twitterStream.addListener(new OMOStatusListener());
        twitterStream.sample();
    }

    public static void example4() throws TwitterException {
        TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
        twitterStream.addListener(new FilteringOMOStatusListener());
        twitterStream.sample();
    }

    public static void example5() throws TwitterException {
        TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
        twitterStream.addListener(new LanguageStatisticsOMOStatusListener(50));
        twitterStream.sample();
    }

    public static void main(String[] args) throws TwitterException {
        example5();
    }
}